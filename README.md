# angular-workshop — AngularJS Workshop App

Clonare git repo:

```
git clone https://bitbucket.org/corley/angular-workshop-advanced.git
cd angular-workshop-advanced
```

### Installare le dipendenze

Eseguire il comando:

```
sudo npm -g install grunt-cli karma bower
npm install --force
```

Il comando npm install può richiedere un po' di tempo. Assicurarsi che sia stato correttamente completatoverificando l'esistenza del file "config/secret.json" che viene generato a termine installazione.

### Avviare e testare l'app:

Eseguire il comando

```
grunt watch:web
```

Quindi aprire il browser e navigare `http://localhost:8081` o `http://127.0.0.1:8081`, dovrebbe essere visibile una pagina d'esempio in html e angular.
Se l'ambiente è correttamente configurato, un messaggio di conferma verrà visualizzato.
